'use strict';

import React, { Component } from 'react';

let logoKeyrock = "./public/images/logo01.png" 

export default class Step0 extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    return (
      <div className="step step0">
        <div className="row">

                <div className="col s12 m12 l12 xl12">

                    <div className="row">
                    <div className="col offset-s4 s6 offset-m4 m6 offset-l4 l6 offset-xl4 xl6">
                        <p>Dear [investor name] Welcome to Keyrock,</p> 
                        <p>You have been invited to join a selected group of investors that understand the current market
                        and see opportunities where others don't.</p>
                        <p>Follow the process to complete your registration and get started</p>
                        <p>The Keyrock team</p>
                        <img src={logoKeyrock} alt="Logo Keyrock"/>
                    </div>
                    </div>
                </div>

              </div>
            </div>     
    )
  }
}
