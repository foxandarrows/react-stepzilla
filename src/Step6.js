'use strict';

import React, { Component } from 'react';
import Dropzone from 'react-dropzone';

export default class Step6 extends Component {
  constructor(props) {
    super(props);
    
    this.state = {
      disabled : true,
      value_first_name : "",
      value_family_name : "",
      value_passport_number : "",
      value_passport_name : "",
      value_phone_number : "",
      value_address : "",
      value_city : "",
      value_zip : "",
      value_country : ""
    };

    this.handleSubmit = this.handleSubmit.bind(this);

    this.handleToogle = this.handleToogle.bind(this);

    this.handleValueChangeFirstName = this.handleValueChangeFirstName.bind(this);
    this.handleValueChangeFamilyName = this.handleValueChangeFamilyName.bind(this);
    this.handleValueChangePassportNumber = this.handleValueChangePassportNumber.bind(this);
    this.handleValueChangePassportName = this.handleValueChangePassportName.bind(this);
    this.handleValueChangePhoneNumber = this.handleValueChangePhoneNumber.bind(this);
    this.handleValueChangeAddress = this.handleValueChangeAddress.bind(this);
    this.handleValueChangeCity = this.handleValueChangeCity.bind(this);
    this.handleValueChangeZip = this.handleValueChangeZip.bind(this);
    this.handleValueChangeCountry = this.handleValueChangeCountry.bind(this);

    this.handleFileUpload = this.handleFileUpload.bind(this);
  }

  handleSubmit(e) {
    alert('Your details have been sent');
    e.preventDefault();
  }

  handleValueChangeFirstName(e) {
      this.setState({
        value_first_name : e.target.value,
      });
  }

  handleValueChangeFamilyName(e) {
    this.setState({
      value_family_name : e.target.value,
    });
  }

  handleValueChangePassportNumber(e) {
    this.setState({
      value_passport_number : e.target.value,
    });
  }

  handleValueChangePassportName(e) {
    this.setState({
      value_passport_name : e.target.value,
    });
  }

  handleValueChangePhoneNumber(e) {
    this.setState({
      value_phone_number : e.target.value,
    });
  }

  handleValueChangeAddress(e) {
    this.setState({
      value_address : e.target.value,
    });
  }

  handleValueChangeCity(e) {
    this.setState({
      value_city : e.target.value,
    });
  }

  handleValueChangeZip(e) {
    this.setState({
      value_zip : e.target.value,
    });
  }

  handleValueChangeCountry(e) {
    this.setState({
      value_country : e.target.value,
    });
  }

  handleToogle(e) {
    if(this.state.value_first_name &&
      this.state.value_family_name &&
      this.state.value_passport_number &&
      this.state.value_passport_name &&
      this.state.value_phone_number &&
      this.state.value_address &&
      this.state.value_city &&
      this.state.value_zip &&
      this.state.value_country !== "" ){
      this.setState({disabled : false})
    } else {
      this.setState({disabled : true})
    }  
  }

  handleFileUpload(){
    function onDrop(acceptedFiles, rejectedFiles) {
      {/* Filippo magic hand to code */}
    }
  }

  render() {
    return (
      <div className={"step step6 enable-" + this.state.disabled}>
        <div className="row">

                <div className="col offset-s2 s8  offset-m2 m8 offset-l2 l8 offset-xl2 xl8">
                  <div className="row">
                  <form onSubmit={this.handleSubmit}>

                    {/* First Name */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="first_name">First Name</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="Your first name" id="first_name" type="text" 
                      className="validate" 
                      value={this.state.value_first_name}
                      onChange={(e) => {this.handleValueChangeFirstName(e) ; this.handleToogle(e)}}
                      required />
                      {/* <label for="first_name">First Name</label> */}
                    </div>

                    {/* Family Name */}
                    <div class="input-field col s4 m4 l4 xl4">
                      <label for="last_name">Family Name</label>
                    </div>
                    <div class="input-field col s8 m8 l8 xl8">
                      <input placeholder="Your family name" id="last_name" type="text"
                      className="validate"
                      value={this.state.value_family_name}
                      onChange={(e) => {this.handleValueChangeFamilyName(e) ; this.handleToogle(e)}}
                      required />
                      {/* <label for="last_name">Family Name</label> */}
                    </div>

                    {/* Passport number */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="passport_number">Passport number</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="Your passport number" id="passport_number" type="text" className="validate"
                      value={this.state.value_passport_number}
                      onChange={(e) => {this.handleValueChangePassportNumber(e) ; this.handleToogle(e)}}
                      required />
                    </div>
                    {/* Passport picture */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="passport_picture">Passport picture</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8" style={{"paddingTop" : "0.75em"}}>
                      {/*<input placeholder="Your passport picture" id="passport_picture" type="text" className="validate"
                      
                      required />*/}
                      <input type="file" 
                      onChange={this.handleFileUpload}
                      value={this.state.value_passport_name}
                      onChange={(e) => {this.handleValueChangePassportName(e) ; this.handleToogle(e)}} />
                    </div>
                    

                    {/* Phone number */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="phone">Phone</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="Your phone number" id="phone" type="text" className="validate"
                      value={this.state.value_phone_number}
                      onChange={(e) => {this.handleValueChangePhoneNumber(e) ; this.handleToogle(e)}}
                      required />
                    </div>
                    {/* Warn user that a phone verification will be needed for any withdraw so make sure that the number is valid */}

                    {/* Address */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="address">Address</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="Street + n°" id="address" type="text" className="validate"
                      value={this.state.value_address}
                      onChange={(e) => {this.handleValueChangeAddress(e) ; this.handleToogle(e)}}
                      required />
                    </div>

                    {/* City */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="city">City</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="City" id="city" type="text" className="validate"
                      value={this.state.value_city}
                      onChange={(e) => {this.handleValueChangeCity(e) ; this.handleToogle(e)}}
                      required />
                    </div>

                    {/* ZIP */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="zip">ZIP</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="ZIP Code" id="zip" type="text" className="validate"
                      value={this.state.value_zip}
                      onChange={(e) => {this.handleValueChangeZip(e) ; this.handleToogle(e)}}
                      required />
                    </div>
                    
                    {/* Country */}
                    <div className="input-field col s4 m4 l4 xl4">
                      <label for="country">Country</label>
                    </div>
                    <div className="input-field col s8 m8 l8 xl8">
                      <input placeholder="Country" id="country" type="text" className="validate"
                      value={this.state.value_country}
                      onChange={(e) => {this.handleValueChangeCountry(e) ; this.handleToogle(e)}}
                      required />
                    </div>

                    <div className="right-align">
                      <input type="submit" value="Submit" id="submit"
                      onClick={this.handleSubmit}/>
                    </div>

                  </form>
                  </div>
                </div>
        </div>
      </div>     
    )
  }
}
