'use strict';

import React, { Component } from 'react';

export default class Step3 extends Component{
  constructor(props){
    super(props);
    this.handleToogle = this.handleToogle.bind(this);
    this.nextToogle = this.nextToogle.bind(this);
    // this.state = {hide : true};
    this.state = {
      disabled : true,
      hide: false
    };
    // this.state = this.props.donneMonStore.step3.hide;
  }

  handleToogle() {
    // this.props.donneMonStore.step3.hide = !this.props.donneMonStore.step3.hide
    this.setState({hide: !this.state.hide});
  }
  nextToogle(){
    this.setState({disabled : !this.state.disabled});
  }

  render(){
      return(
        <div className={"step step3 enable-" + this.state.disabled}>
        <div className="row">
        <div>
          <div className="col offset-s1 s10 offset-m1 m10 offset-l1 l10 offset-xl1 xl10">

          <h5 className="center-align">TERMS OF USE</h5>
          <br />
          <iframe width="100%" height="650px" src="https://docs.google.com/document/d/e/2PACX-1vS9gSrd0oEeU1NvH8FjlmLBe291uToSAkF6duEHr6O1Rgnv3Xcz4EfJ6usrxqPeqih0lgkzOB-Yz1fw/pub?embedded=true"></iframe>

          <input type="checkbox" id="understand" onClick={(event) => { this.handleToogle(); this.nextToogle();}}/>
          <label for="understand">
            <span>I understand</span>
          </label>
          {/*<a className="waves-effect waves-light btn" onClick={this.nextToogle}><span onClick={this.handleToogle}>I understand</span></a> */}
          
          {/* {this.props.doneMonStore.step3.hide} */}
          <div className={"show-" + this.state.hide}> 
          {/* this.props.donneMonStore.step3.hide */}
            <div className="center-align">
              <h6>
                <span style={{"color": "white"}}>
                <br />
                A version of this contract has been sent to your email box!
                </span>
                <br />
              </h6>
            </div>

            </div>
        </div>
      </div>
      </div>
      </div>
      )
  }
}

    





  
                      


                  
