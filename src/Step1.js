'use strict';

import React, { Component } from 'react';
import Figure1 from './img/step1Figure1.png';
import Figure2 from './img/step1Figure2.png';
import Figure3 from './img/step1Figure3.png';


export default class Step1 extends Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  componentDidMount() {}

  componentWillUnmount() {}

  // not required as this component has no forms or user entry
  // isValidated() {}

  render() {
    return (
      <div className="step step1">
        <div className="row">

                <div className="col offset-s1 s10 offset-m1 m10 offset-l1 l10 offset-xl1 xl10">

                  <div> 
                    <span className="border"><h5>Summary</h5></span>
                    <p>Blockchain technologies and cryptocurrencies are a revolution in the way we represent/transfer value
                    and structure business models. With a market capitalization of 200 billion USD (for all crypto assets combined), 
                    more than 150 billion USD monthly trading volume and more that 3 successful ICOs (initial coin offerings) every day 
                    the industry has rapidly become a new asset-class such as Forex, commodities or stocks. 
                    </p>
                    <p>
                    In the same way the other traditional markets require capital infrastructure (exchanges, transfer mechanisms, 
                    custodian systems, issuance, regulation, brokerage, settlement, market-makers , etc ), crypto-assets require capital
                    infrastructure adapted to their needs to achieve mass adoption.   
                    </p>
                    <p>
                    EDSE’s objective is to make the crypto-asset market fair and efficient for all investors. 
                    </p>
                    <p>
                    The industry is young and due to the decentralized nature of the assets, liquidity is highly fragmented, with more than 
                    150 active exchanges and 1200 tokens, the need for optimization and the creation of infrastructure is pressing.
                    </p>
                    <p>
                    The Liquidity Network is a product of EDSE that allows investors to contribute to a fund (in ETH) that is used to provide smart 
                    market-making on the various crypto exchanges (already in partnership with Huobi, Binance,  Bitfinex, Bittrex, Qryptos and Gatecoin). 
                    It provides investors in the fund a passive income by giving them the ROI generated by the operations.
                    </p>
                  </div>
                  <br />

                  <div>
                     <span className="border"><h5>Liquidity</h5></span>
                     <p>
                      Liquidity refers to the degree to which a certain asset (cash, securities, cryptocurrency) can be quickly bought or sold in the market without affecting the asset's 
                      price. This is largely dependant on the amount of an asset available to trade on an order book.  An example of a crypto asset pair (NEO/ETH) 
                      being traded on an order book can be found here. 
                     </p>
                  </div>
                  <br />

                  <div>
                      <span className="border"><h5>Liquidity providers - Market Makers</h5></span>
                      <p>
                      Liquidity providers, or also known as market-makers, are actors who hold and trade large quantities of a given asset on a particular exchange. Trades facilitated by market-makers
                      help maintain an asset’s price, prevent market manipulation, bar arbitrage,  provide opportunity for larger trades, and ultimately create a more competitive and healthier market.   
                      </p>
                      <p>
                      We build a network of exchanges, developing a trusted relationships with them, allowing for privileged trading conditions (reduced fees, on-premises servers…).<br/>
                      We use in-house trading algorithms and bots to merge exchanges’ order books and allow them to benefit from one another’s liquidity
                      </p>
                      <p>
                      A market maker is a broker-dealer firm that assumes the risk of holding a certain number of shares of a particular security in order to facilitate the trading of that security.<br/>
                      There are more than 500 member firms that act as market makers for Nasdaq alone, and therefore keeping the financial markets running efficiently.<br/>
                      In the crypto industry the service is just in its infancy with a couple of players worldwide.<br/> 
                      </p>
                  </div>
                  <br />

                  <div>
                      <span className="border"><h5>The liquidity problem</h5></span>
                      <p>
                      Due to the decentralized nature of crypto assets, they tend to be traded on a multitude of exchanges which fragments liquidity. 
                      A sparse liquidity network creates several challenges for the crypto exchange ecosystem, such as; arbitrage, market manipulation, an inability to fulfill large trades, 
                      and a greater difficulty to trade a diverse collection of assets on one exchange. <br />
                      Figure 1 shows the distribution of liquidity for one pair among multiple exchanges. The size of the circles represents their trading volume.
                      </p>
                      <div className="center-align">
                        <img src={Figure1} alt="figure1" title="figure1" id="img-figure1"/>
                      </div>
                      <p>
                      Market making is currently done by the exchanges themselves or a third party intermediary. <br/>
                      Figure 2 displays a typical mapping of how a crypto exchange network in relation to market-makers looks today. The traded pair shown is OMG/ETH.
                      </p>
                      <div className="center-align">
                        <img src={Figure2} alt="figure2" title="figure2" className="img-figure"/>
                      </div>
                  </div>
                  <br />

                  <div>
                    <span className="border"><h5>Understanding of the risk</h5></span>
                    <p>By depositing funds in the EDSE platform you agree to give full control of your funds to be invested according to the transparant EDSE market
                       making strategy. This means that you will profit from the upsides of the operation in the same way you take the risk attached to possible downturns in the operation. 
                    </p>
                  </div>
                  <br />
                  <br />

                </div>

              </div>
            </div>     
    )
  }
}
