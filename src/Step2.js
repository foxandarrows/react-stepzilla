'use strict';

import React, { Component } from 'react';


export default class Step2 extends Component{

  constructor(props) {
    super(props);
    this.handleToogle = this.handleToogle.bind(this);
    this.state = {
      // disabled = document.getElementById("next-button").disabled = true
      disabled : true
    };
  }
  handleToogle() {
    // disabled = document.getElementById("next-button").disabled = true;
    this.setState({disabled: !this.state.disabled});
  }

  render(){
    return(
      <div className={"step step2 enable-" + this.state.disabled}>
      <div className="row">

        <div className="col offset-s1 s10 offset-m1 m10 offset-l1 l10 offset-xl1 xl10">
                  <div>
                  <span className="border"><h5>Deposit</h5></span>
                    <p>Minimum initial deposit 80 ETH send to the EDSE ETH address <br/>
                    0 Deposit fees <br/>
                    Only ETH accepted <br/>
                    </p>
                  </div>

                  <div>
                    <span className="border"><h5>Withdrawal</h5></span>
                    <p>
                    Need to be confirmed verbally by the investor and are provided between 3 hours to 4 days 
                    depending of the withdrawal times of the exchanges in which the funds are providing market-making services. <br />
                    Withdrawal fees - 0.1 ETH (including Ethereum network fees)
                    </p>
                  </div>

                  <div>
                    <span className="border"><h5>Funds Allocation</h5></span>
                    <p>The fund will be used for trading in ETH/NEO pairs in multiple crypto exchanges such as Bitfinex, Bittrex, Binance,
                    Qryptos, Huobi, Gatecoin. <br />
                    You will be informed when your funds are used for future pairs.
                    </p>
                  </div>

                  <div>
                    <span className="border"><h5>Return on investment</h5></span>
                    <p>
                    The returns of the fund are published at the end of every week (Friday at 4pm).<br/>
	                  EDSE does not guarantee any return on investment.
                    </p>
                  </div>

                  <div>
                    <span className="border"><h5>Requirement</h5></span>
                    <p>
                    Investor must provide KYC information, passport and residence information.<br/>
	                  Investor must deposit min 80 ETH in the EDSE platform
                    </p>
                  </div>

                  <div>
                    <span className="border"><h5>Fund fees</h5></span>
                    <p>
                    No fixed management fees <br />
	                  No fixed cost <br />
	                  The fund holds 30% of the revenue and returns 70% to the investor 
                    </p>
                  </div>

                  <form action="#">
                          <br />
                          <p id="agreement">
                          I confirm that I have a knowledge about financial investments and that I understand 
                          the role of EDSE in the crypto-asset ecosystem.
                          </p>

                          <div>
                            <input type="checkbox" id="understand" onClick={this.handleToogle}/>
                            <label for="understand">I understand</label>
                          </div>
                          <br />
                  </form>
        </div>
      </div>
    </div>
    )
  }
}


  
                      


                  
