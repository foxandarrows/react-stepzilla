import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Steps from './Steps';

//import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(<Steps />, document.getElementById('root'));
//registerServiceWorker();
