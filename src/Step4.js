'use strict';

import React from 'react';

const Step4 = (props) => (
    <div className="step step4">
      <div className="row">
      <div className="col offset-s1 s10 offset-m1 m10 offset-l1 l10 offset-xl1 xl10">
        <br />
        <span className="border"><h5>Trading pairs</h5></span>
        <p>Your funds on the Liquidity Network will work solely for the ETH/NEO trading pair.</p>

        <span className="border"><h5>NEO</h5></span>
        <p>NEO has been called “China’s Ethereum”. The Western market first collectively learned about 
          Antshares, a smart contract and decentralized application (dApp) platform, just ahead of the 
          company’s rebrand to ‘NEO’. Prices skyrocketed in the weeks ahead of the rebrand. Antshares’ ICO was in the fall of 2016.
        </p>
        <p>
        NEO is a strong Protocol and already has a solid product, team and use cases by other projects issuing 
        their ICOs on top of it.
        </p>

        <span className="border"><h5>ETH</h5></span>
        <p>
        is a protocol of decentralized exchanges allowing the creation by the users of intelligent contracts thanks
        to a Turing-complete language. These smart contracts are based on a computer protocol to verify or implement a mutual contract, 
        they are deployed and publicly available in the blockchain.
        </p>
        <p>
        Ethereum uses a unit of account called Ether as a means of payment of these contracts. Its corresponding acronym, 
        used by the exchange platforms, is "ETH". Ethereum is the second largest decentralized cryptographic currency with a capitalization 
        of more than 25 billion Euros.
        </p>
        <p>
        Your deposits are solely in ETH but at any given point in time they are split and exchanged into NEO. The reports provided by EDSE
        are therefore an approximation of your outstanding balance, but since the price of NEO moves in time there might be a minimal discrepancy 
        between withdrawals and outstanding funds in the account.
        </p>

      </div>
      </div>
    </div>
)

export default Step4;

