import React from 'react';
import ReactDOM from 'react-dom';
import {CopyToClipboard} from 'react-copy-to-clipboard';
import './Copy.css';
 
export default class Copy extends React.Component {

  constructor(props) {
    super(props);
    // this.handleToogle = this.handleToogle.bind(this);
    this.state = {
    value: 'QSDFLJ2435LF',
    copied: false,
    };
  }

  // handleToogle() {
  //   this.setState({copied: !this.state.copied});
  // }

  render() {
    return (
      <div>
       
        <CopyToClipboard text={this.state.value}
          onCopy={() => this.setState({copied: true})}>
          <span id="copy-deposit" className="right-align col offset-s1 s5 offset-m1 m5">
            <a>Copy</a>
          </span>
        </CopyToClipboard>

        <input id="copy-value" className="left-align col offset-s1 s5 offset-m1 m5" value={this.state.value}
          onChange={({target: {value}}) => this.setState({value, copied: false})}
          readOnly/>
        
        <span>
          {this.state.copied ? <span style={{color: '#2aba9d'}}>Copied!</span> : null}
        </span>

      </div>
    );
  }
}
 





