// 'use strict';

import React, { Component } from 'react';
//import StepZilla from '../main'
import Step0 from './Step0';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import Step4 from './Step4';
import Step5 from './Step5';
import Step6 from './Step6';

import 'materialize-css'; // It installs the JS asset only
import 'materialize-css/dist/css/materialize.min.css';

import './Steps.css';
import './js/script.js';

var StepZilla = require('react-stepzilla').default;
// https://stackoverflow.com/questions/34130539/uncaught-error-invariant-violation-element-type-is-invalid-expected-a-string

export default class Steps extends Component {
  constructor(props) {
    super(props);

    // this.sampleStore = {
    //   step3 : {
    //     hide : true
    //   }
    // };
  }

  componentDidMount() {}

  componentWillUnmount() {}

  getStore() {
    return this.sampleStore;
  }

  updateStore(update) {
    this.sampleStore = {
      ...this.sampleStore,
      ...update,
    }
  }

  render() {
    const steps =
    [
      {name: 'Welcome', component: <Step0 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />},
      {name: 'Abstract', component: <Step1 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />},
      {name: 'Fees', component: <Step2 getStore={this.sampleStore} updateStore={(u) => {this.updateStore(u)}}/>},
      {name: 'Agreement', component: <Step3 donneMonStore={this.sampleStore} />},
      {name: 'Pairs', component: <Step4 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />},
      {name: 'Settings', component: <Step6 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />},
      {name: 'Wallet', component: <Step5 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />},
      // {name: 'Confirmation', component: <Step6 getStore={() => (this.getStore())} updateStore={(u) => {this.updateStore(u)}} />}
    ]

    return (
      <div className='example'>
        <div className='step-progress'>
          <StepZilla
            steps={steps}
            showNavigation={true}
            showSteps={true}
            stepsNavigation={true}
            prevBtnOnLastStep={false}
            preventEnterSubmission={true}
            nextTextOnFinalActionStep={"Save"}
           />
        </div>
      </div>
    )
  }
}
