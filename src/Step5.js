import React, { Component } from 'react';
import ethereum from './img/logo-ethereum.png';
import 'materialize-css'; // It installs the JS asset only
import 'materialize-css/dist/css/materialize.min.css';
import './component/Wallet/Wallet.css';
import './component/Copy/Copy.css';
import Copy from './component/Copy/Copy.js';
import QRcode from './component/QRcode/QRcode.js';

class Step5 extends Component {

    constructor(props) {
        super(props);
        // this.handleToogle = this.handleToogle.bind(this);
        // this.state = {hide: true};
    }

    // handleToogle() {
    //     this.setState({hide: !this.state.hide});
    // }

  render() {
    return (
    <div className="container row">

        <div className="col offset-s5 s7 offset-m5 m7 offset-l5 l7 offset-xl5 xl7">

            <div id="presentation" className="center-align">
                <p>To get started, make a deposit!</p>
            </div>
            <div id="title-wallet" className="left-align">
                <img id="icon-ethereum" src={ethereum} />
                <h3>Ethereum Wallet</h3>
            </div>

            <div id="wallet" className="center-align">
                
                <div id="total-balance">
                    <p id="title-total-balance">Total balance</p>
                    <h3>0.95 ETH</h3>
                    <h5>254.56 euros</h5>
                </div>
            </div>

            <div id="footer-wallet" className="center-align">
                {/* <button id="deposit">Deposit</button> */}
                {/* onClick={this.handleToogle} */}
                {/* {this.state.hide ? <Copy /> : <QRcode /> } */}
                {/* <div className={'hide-' + this.state.hide}> */}
                    <br/>
                    <Copy />
                    <br /><br />
                    <QRcode />
            </div>
        </div>

        <div className="right-align">
            <a href="https://edse.eu/" style={{"marginTop" : "2em"}} className="btn right-align">Continue</a>
            <br />
        </div>

    </div>

    
    );
  }
}

export default Step5;


    





  
                      


                  
